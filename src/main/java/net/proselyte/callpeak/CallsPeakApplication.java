package net.proselyte.callpeak;

import net.proselyte.callpeak.service.CallService;
import net.proselyte.callpeak.service.CallServiceImpl;

public class CallsPeakApplication {
	public static void main(String[] args) {
		CallService callService = new CallServiceImpl();
		callService.calculateMaxSimultaneousCallsPeriod();
	}
}