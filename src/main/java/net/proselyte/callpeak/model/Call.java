package net.proselyte.callpeak.model;

/**
 * Simple object that represents a Call.
 *
 * @author Eugene Suleimanov
 */
public class Call {
    private long callStart;
    private long callEnd;

    public Call(long start, long callEnd) {
        this.callStart = start;
        this.callEnd = callEnd;
    }

    public long getCallStart() {
        return callStart;
    }

    public long getCallEnd() {
        return callEnd;
    }
}