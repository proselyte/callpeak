package net.proselyte.callpeak.model;

import java.util.Comparator;

/**
 * Object that represents an Event that contains call data.
 * Designed to define is defined {@link Call} included to defined intersection.
 *
 * @author Eugene Suleimanov
 */
public class Event {

    private long startTime;
    private long endTime;
    private boolean isStartTime; //if start = true
    private int intersection;

    public Event(long startTime, boolean isStartTime) {
        this.startTime = startTime;
        this.isStartTime = isStartTime;
    }

    public long getStartTime() {
        return startTime;
    }


    public boolean isStartTime() {
        return isStartTime;
    }

    public int getIntersection() {
        return intersection;
    }

    public void setIntersection(int intersection) {
        this.intersection = intersection;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public static final Comparator<Event> EVENT_COMPARATOR = (event1, event2) -> (int) (event1.getStartTime() - event2.getStartTime());

    public static final Comparator<Event> INTERSECTION_COMPARATOR = (event1, event2) -> (event2.getIntersection() - event1.getIntersection());
}
