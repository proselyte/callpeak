package net.proselyte.callpeak.service;

/**
 * Service interface that conatins method for calculating max amount of simultaneous calls.
 *
 * @author Eugene Suleimanov
 */
public interface CallService {

    /**
     * Calculates maximum amount of simultaneous calls.
     */
    void calculateMaxSimultaneousCallsPeriod();
}
