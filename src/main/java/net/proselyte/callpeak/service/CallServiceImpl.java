package net.proselyte.callpeak.service;

import net.proselyte.callpeak.dao.CallDAO;
import net.proselyte.callpeak.dao.JavaIOCallDAOImpl;
import net.proselyte.callpeak.model.Call;
import net.proselyte.callpeak.model.Event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of {@link CallService} interface.
 *
 * @author Eugene Suleimanov
 */
public class CallServiceImpl implements CallService {

    CallDAO callDAO = new JavaIOCallDAOImpl();

    @Override
    public void calculateMaxSimultaneousCallsPeriod() {
        Long startTime = System.currentTimeMillis();

        List<Event> highDiapasons = new ArrayList<>();
        List<Event> events = new ArrayList<>();

        int enumerator = 0;

        for (Call call : callDAO.listCalls()) {
            events.add(new Event(call.getCallStart(), true));
            events.add(new Event(call.getCallEnd(), false));
        }

        Collections.sort(events, Event.EVENT_COMPARATOR);

        for (int i = 0; i < events.size(); i++) {

            if (events.get(i).isStartTime()) {
                events.get(i).setEndTime(events.get(i + 1).getStartTime());
                enumerator++;
                events.get(i).setIntersection(enumerator);
            } else {
                enumerator--;
                events.get(i).setIntersection(enumerator);
            }
        }

        Collections.sort(events, Event.INTERSECTION_COMPARATOR);

        if (events.size() > 0) {
            highDiapasons.add(events.get(0));
            for (int i = 1; i < events.size(); i++) {
                if (events.get(i).getIntersection() == highDiapasons.get(0).getIntersection()) {
                    highDiapasons.add(events.get(i));
                }
            }
        }

        for (Event event : highDiapasons) {
            System.out.println("\n===================================================================");
            System.out.println("The peak for this call log is " + event.getIntersection() + " simultaneous calls, that occurred between "
                    + event.getStartTime() + " and " + event.getEndTime());
            System.out.println("===================================================================\n");
        }


        Long endTime = System.currentTimeMillis();
        Long totalTimeSpent = endTime - startTime;

        System.out.println("Time spent: " + totalTimeSpent + " ms");
    }
}
