package net.proselyte.callpeak.dao;


import net.proselyte.callpeak.model.Call;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Implementation of {@link CallDAO} that provides access to the text file via Java I/O.
 */

public class JavaIOCallDAOImpl implements CallDAO {

    private static final String FILE_PATH = "src/main/resources/callLog.txt";

    @Override
    public List<Call> listCalls() {
        List<Call> result = new ArrayList<>();
        Scanner scannerLog = null;

        try {
            scannerLog = new Scanner(new File(FILE_PATH));
            while (scannerLog.hasNext()) {
                String[] logs = scannerLog.next().split(":");
                Call newCall = new Call(Long.valueOf(logs[0]), Long.valueOf(logs[1]));
                result.add(newCall);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            scannerLog.close();
        }

        return result;
    }
}
