package net.proselyte.callpeak.dao;

import net.proselyte.callpeak.model.Call;

import java.util.List;


/**
 * DAO interface that provides access to the storage for {@link Call} entity.
 */
public interface CallDAO {

    /**
     * Reads file with call logs and transforms it to the List of {@link Call}s.
     *
     * @return calls
     */
    List<Call> listCalls();
}
